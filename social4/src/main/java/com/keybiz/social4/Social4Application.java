package com.keybiz.social4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Social4Application {

	public static void main(String[] args) {
		SpringApplication.run(Social4Application.class, args);
	}

}

