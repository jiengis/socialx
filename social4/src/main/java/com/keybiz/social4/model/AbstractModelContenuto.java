package com.keybiz.social4.model;

import java.util.Date;

//import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EqualsAndHashCode
public abstract class AbstractModelContenuto {

	@Id
	@GeneratedValue
	private Long id;

	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;


}
