package com.keybiz.social4.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue("articolo")
public class Articolo extends AbstractModelContenuto {

	private String sezione;
	private Double prezzo;
	private boolean vendibile;
	
	
}
