package com.keybiz.social4.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Commento {

	@Id
	@GeneratedValue
	private Long id;
	private String testo;
	

}
