package com.keybiz.social4.model;


import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@DiscriminatorValue("notizia")
@EqualsAndHashCode(callSuper = true)
public class Notizia extends AbstractModelContenuto {

	private String sottotitolo;
	private String riassunto;
	private Date dataFineVisual;
	private String importanza;
	

	
	
}
