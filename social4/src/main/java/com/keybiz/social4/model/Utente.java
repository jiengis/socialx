package com.keybiz.social4.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Utente {

	@Id
	@GeneratedValue
	private Long id;
	
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private Date dob;
	private String privileges;
	private Double contoVirtuale;
	

	
	
}


